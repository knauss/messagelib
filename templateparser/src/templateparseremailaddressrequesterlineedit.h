/*
   SPDX-FileCopyrightText: 2017-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef TEMPLATEPARSEREMAILADDRESSREQUESTERLINEEDIT_H
#define TEMPLATEPARSEREMAILADDRESSREQUESTERLINEEDIT_H

#include <TemplateParser/TemplateParserEmailAddressRequesterBase>
#include "templateparser_private_export.h"
class QLineEdit;
namespace TemplateParser {
class TEMPLATEPARSER_TESTS_EXPORT TemplateParserEmailAddressRequesterLineEdit : public TemplateParser::TemplateParserEmailAddressRequesterBase
{
    Q_OBJECT
public:
    explicit TemplateParserEmailAddressRequesterLineEdit(QWidget *parent = nullptr);
    ~TemplateParserEmailAddressRequesterLineEdit() override;

    Q_REQUIRED_RESULT QString text() const override;
    void setText(const QString &str) override;
    void clear() override;
private:
    QLineEdit *mLineEdit = nullptr;
};
}
#endif // TEMPLATEPARSEREMAILADDRESSREQUESTERLINEEDIT_H
