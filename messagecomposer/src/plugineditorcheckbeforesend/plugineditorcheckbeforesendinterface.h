/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef PLUGINEDITORCHECKBEFORESENDINTERFACE_H
#define PLUGINEDITORCHECKBEFORESENDINTERFACE_H

#include <QObject>
#include "messagecomposer_export.h"
#include "plugineditorcheckbeforesendparams.h"

namespace MessageComposer {
class PluginEditorCheckBeforeSendInterfacePrivate;
class PluginEditorCheckBeforeSendParams;
/**
 * @brief The PluginEditorCheckBeforeSendInterface class
 * @author Laurent Montel <montel@kde.org>
 */
class MESSAGECOMPOSER_EXPORT PluginEditorCheckBeforeSendInterface : public QObject
{
    Q_OBJECT
public:
    explicit PluginEditorCheckBeforeSendInterface(QObject *parent = nullptr);
    ~PluginEditorCheckBeforeSendInterface();

    virtual bool exec(const MessageComposer::PluginEditorCheckBeforeSendParams &params) = 0;

    void setParentWidget(QWidget *parent);
    Q_REQUIRED_RESULT QWidget *parentWidget() const;

    void setParameters(const MessageComposer::PluginEditorCheckBeforeSendParams &params);
    Q_REQUIRED_RESULT MessageComposer::PluginEditorCheckBeforeSendParams parameters() const;

public Q_SLOTS:
    virtual void reloadConfig();

private:
    PluginEditorCheckBeforeSendInterfacePrivate *const d;
};
}

#endif // PLUGINEDITORCHECKBEFORESENDINTERFACE_H
