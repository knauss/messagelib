/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef WEBHITTESTRESULTTEST_H
#define WEBHITTESTRESULTTEST_H

#include <QObject>

class WebHitTestResultTest : public QObject
{
    Q_OBJECT
public:
    explicit WebHitTestResultTest(QObject *parent = nullptr);
    ~WebHitTestResultTest();
private Q_SLOTS:
    void shouldHaveDefaultValues();
    void shouldAssignPosAndUrl();
    void shouldAssignFromQVariant();
    void shouldCopyWebHitTestResult();
};

#endif // WEBHITTESTRESULTTEST_H
