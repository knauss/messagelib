/*
   SPDX-FileCopyrightText: 2016-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef TESTWEBENGINE_H
#define TESTWEBENGINE_H

#include <QWidget>
#include <QWebEngineView>
namespace WebEngineViewer {
class WebEnginePage;
class WebHitTestResult;
}

class TestWebEngineView : public QWebEngineView
{
    Q_OBJECT
public:
    explicit TestWebEngineView(QWidget *parent = nullptr);
protected:
    void contextMenuEvent(QContextMenuEvent *e) override;
private Q_SLOTS:
    void slotHitTestFinished(const WebEngineViewer::WebHitTestResult &result);
};

class TestWebEngine : public QWidget
{
    Q_OBJECT
public:
    explicit TestWebEngine(QWidget *parent = nullptr);
    ~TestWebEngine();
private:
    WebEngineViewer::WebEnginePage *mEnginePage;
};

#endif // TESTWEBENGINE_H
